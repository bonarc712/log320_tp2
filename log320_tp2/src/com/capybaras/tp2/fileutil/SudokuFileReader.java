package com.capybaras.tp2.fileutil;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SudokuFileReader {

	//private static Reader reader;
	
	public static ArrayList<Character> readFile(String fileName) throws IOException {
		ArrayList<Character> arrayList = new ArrayList<Character>(81);
		FileInputStream in = new FileInputStream(fileName);
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
		
		try {
			for (int i=0; i<9; i++) {
				String line = reader.readLine();
				if (line.length() > 9)
					throw new IOException("Erreur de lecture");
				for (int j=0; j<9; j++) {
					char readChar = line.charAt(j);
					if (readChar == '0' || readChar == '1'
							|| readChar == '2'
							|| readChar == '3'
							|| readChar == '4'
							|| readChar == '5'
							|| readChar == '6'
							|| readChar == '7'
							|| readChar == '8'
							|| readChar == '9')
					arrayList.add(line.charAt(j));
					else
						throw new IOException("Unexpected character");
				}
			}
			reader.close();
			return arrayList;
		}
		catch(IOException e) {
			System.out.println("Erreur lors de la lecture du fichier sudoku: " + e.toString());
			arrayList.clear();
			reader.close();
			return arrayList;
		}
		finally {
			reader.close();
		}
	}
	
}
