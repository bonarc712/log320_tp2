package com.capybaras.tp2.core;

import java.util.List;

import org.apache.commons.lang.time.StopWatch;

public class ResolverController {
	
	private Sudoku _sudoku;
	private SudokuStack _stack;
	private boolean _noSolution;
	private int _testCounter = 0;
	
	public ResolverController(Sudoku sudoku) {
		_sudoku = sudoku;
		_stack = new SudokuStack();
	}
	
	public void resolve() {
		StopWatch watch = new StopWatch();
		watch.start();
		System.out.println("Voici le sudoku: \n"+_sudoku);
		boolean sudokuHasSolution = searchSolution();		
		float currentTime = watch.getTime();
		watch.stop();
		System.out.println("L'exécution du programme a duré " + currentTime/1000 + " secondes.");
		if (sudokuHasSolution) {
			System.out.println("Une solution a été trouvée.");
			System.out.println("Voici la grille compl�te : \n" + _sudoku);
		}
		else
			System.out.println("La grille n'a pas de solution.");
	}
	
	private boolean searchSolution() {
		initializeData();
//		Sudoku aNewSudoku = new Sudoku(_sudoku);
//		_stack.push(_sudoku);
//		_sudoku = aNewSudoku;
		while (!_sudoku.isSudokuEnded()) {
			takeTurn();
		}
		if (_noSolution)
			return false;
		return true;
	}
	
	private void initializeData() {
		_sudoku.initializePossibilities();
	}
//	
//	/***
//	 * 
//	 * Fonction fortement inspirée de la fonction trouvée sur le site
//	 * http://www.colloquial.com/games/sudoku/java_sudoku.html
//	 *  (en fin de compte ça n'a pas fonctionné)
//	 */
//	private boolean solveSudoku(int i, int j, Sudoku grille) {
//		System.out.println("Grille: ");
//		System.out.println(grille);
//		System.out.println("On se rend ici");
//		if (i == 9) {
//			i = 0;
//			j++;
//			if (j == 9)
//				return true;
//		}
//		Cellule currCell = grille.getCelluleAtIndex(i+j*9);
//		if (currCell.getCharacter() != '0')
//			return solveSudoku(i+1, j, grille);
//		
//		for (int k=1; k<10; k++) {
//			String aString = k + "";
//			char aChar = aString.charAt(0);
//			if (_sudoku.checkIfCharIsGood(currCell, aChar)) {
//				currCell.setCharacter(aChar);
//				if (solveSudoku(i+1, j, grille))
//					return true;
//			}
//		}
//		currCell.setCharacter('0');
//		return false;
//	}
//	/***
//	 * 
//	 * Fin du code inspiré
//	 * 
//	 */
	
	private void takeTurn() {
		
//		_noSolution = !solveSudoku(0,0,_sudoku);
		if (!_sudoku.isSudokuEnded()) {
			if (!_sudoku.hasMissingCases())
				_sudoku.endSudoku();
		//	boolean directCheckWorked = _sudoku.directCheck();
	//		if (!directCheckWorked) {
////				boolean neighbourCheckWorked = _sudoku.neighbourCheck();
//////				_sudoku.endSudoku();
//////			}
			backTrackTurn();
		}
//			
	}
	
	private void backTrackTurn() {
//		System.out.println("Passage # " + _testCounter);
	//	System.out.println("Stack size: " + _stack.size());
	//	System.out.println("Grille : \n" + _sudoku);
	//	_testCounter++;
		Sudoku aNewSudoku = new Sudoku(_sudoku);
		_stack.push(_sudoku);
		_sudoku = aNewSudoku;
    	if (_sudoku.hasMissingCases()) {
			Cellule nextCell = _sudoku.getNextCelluleToFill();
			if (nextCell.getCharacter() != 'a') {
				System.out.println("sur");
				System.out.println(nextCell.getIndex() + " " + nextCell);
				System.out.println("sous");
				List<Character> chars = nextCell.getPossibilitiesAsList();
				for (int i=0; i<chars.size(); i++) {
					if (i != 0 && !_sudoku.isSudokuEnded()) {
						_sudoku = _stack.pop();
						Sudoku aNewerSudoku = new Sudoku(_sudoku);
						_stack.push(_sudoku);
						_sudoku = aNewerSudoku;
					}
					char currChar = chars.get(i);
					boolean result = nextCell.assignPossibility(currChar);
//					if (result) {
						_sudoku.refreshNeighbouringCells(nextCell);
						_sudoku.trierCellulesToFill();
						_sudoku.removeFromCellulesToFill(0);
						takeTurn();
//					}
//					else {
//						_sudoku = _stack.pop();
//					}
				}
				if (!_sudoku.hasMissingCases())
					_sudoku.endSudoku();
				if (_stack.isEmpty())
					_noSolution = true;
				if (chars.size() == 0) {
					_sudoku = _stack.pop();
				}
			}
			else {
				if (_stack.isEmpty())
					_sudoku.endSudoku();
				else
					_sudoku = _stack.pop();
			}
		}
		else {
			if (!_sudoku.isSudokuEnded()) {
				_sudoku = _stack.pop();
			}
		}
		
	}

}
