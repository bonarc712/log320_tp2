package com.capybaras.tp2.core;

import java.util.Comparator;

public class PossibilitiesCountComparator<T> implements Comparator<T> {
	
	public int compare(Object objet1, Object objet2) {
		Cellule cell1 = (Cellule)objet1;
		Cellule cell2 = (Cellule)objet2;
		return cell1.getPossibilityCount() - cell2.getPossibilityCount();
	}

}
