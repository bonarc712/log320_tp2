package com.capybaras.tp2.core;

import com.capybaras.tp2.ui.SudokuApp;


public class Starter {
	public static void main(String args[]) {
		System.out.println("Sudoku application started..");
		
		SudokuApp application = new SudokuApp();
		application.showOptions();
	}
}
