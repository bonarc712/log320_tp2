package com.capybaras.tp2.core;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Cellule {
	
	private char character;
	private Set<Character> possibilities;
	private boolean hasCharacter;
	private int index; //1 � 81, 0 = pas d'index
	
	public Cellule() {
		possibilities = new TreeSet<Character>();
		hasCharacter = false;
		character = '0';
		index = 0;
		createPossibilitySet();
	}
	
	public Cellule(char character) {
		this.character = character;
		possibilities = new TreeSet<Character>();
		hasCharacter = (character == '0') ? false : true;
		index = 0;
		createPossibilitySet();
	}
	
	public Cellule(char character, int index) {
		this.character = character;
		possibilities = new TreeSet<Character>();
		hasCharacter = (character == '0') ? false : true;
		this.index = index;
		createPossibilitySet();
	}
	
	public Cellule(Cellule cell) {
		this(cell.character, cell.index);
		possibilities = new TreeSet<Character>();
		Iterator<Character> iter = cell.possibilities.iterator();
		while (iter.hasNext()) {
			possibilities.add(iter.next());
		}
	}
	
	private void createPossibilitySet() {
		if (!hasCharacter) {
			possibilities.add('1');
			possibilities.add('2');
			possibilities.add('3');
			possibilities.add('4');
			possibilities.add('5');
			possibilities.add('6');
			possibilities.add('7');
			possibilities.add('8');
			possibilities.add('9');
		}
	}

	public char getCharacter() {
		return character;
	}

	public void setCharacter(char character) {
		this.character = character;
		hasCharacter = (character == '0') ? false : true;
	}
	
	public Set<Character> getPossibilities() {
		return possibilities;
	}
	
	public List<Character> getPossibilitiesAsList() {
		List<Character> liste = new ArrayList<Character>();
		Iterator<Character> iter = possibilities.iterator();
		while (iter.hasNext())
			liste.add(iter.next());
		return liste;
	}
	
	public void removePossibility(char possibility) {
		possibilities.remove(possibility);
	}
	
	public boolean assignPossibility() {
		if (possibilities.size() == 1) {
			Iterator<Character> iter = possibilities.iterator();
			Character missingChar = iter.next();
			character = missingChar;
			hasCharacter = true;
			removePossibility(missingChar);
			return true;
		}
		return false;
	}
	
	public boolean assignPossibility(Character possibilityToKeep) {
		int oldLength = possibilities.size();
		removePossibility(possibilityToKeep);
		int newLength = possibilities.size();
		if (oldLength == newLength)
			return false;
		possibilities = new TreeSet<Character>();
		possibilities.add(possibilityToKeep);
		return assignPossibility();
	}
	
	public String toString() {
		return character+"";
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean hasCharacter() {
		return hasCharacter;
	}
	
	public int getPossibilityCount() {
		return possibilities.size();
	}
	

}
