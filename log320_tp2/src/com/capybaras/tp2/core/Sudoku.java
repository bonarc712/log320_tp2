package com.capybaras.tp2.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Sudoku {
	
	private List<Cellule> _grille;
	private List<Cellule> _cellulesToFill;
	private boolean _endMode;

	public Sudoku() {
		_grille = new ArrayList<Cellule>();
		_cellulesToFill = new ArrayList<Cellule>();
		_endMode = false;
	}
	
	public Sudoku(Sudoku otherSudoku) {
		this();
		for(Cellule cell : otherSudoku.getGrille()) {
			_grille.add(new Cellule(cell));
		}
		for(Cellule cell : otherSudoku._cellulesToFill) {
			int index = cell.getIndex() - 1;
			_cellulesToFill.add(_grille.get(index));
		}
		_endMode = otherSudoku._endMode;
	}
	
	public void removeFromCellulesToFill(int index) {
		if (_cellulesToFill.size() > index)
			_cellulesToFill.remove(index);
	}
	
//	public void removeFromCellulesToFill(Cellule cell) {
//		_cellulesToFill.remove(cell);
//	}
	
	public void setGrille(ArrayList<Character> grille) {
		for (int i=0; i<grille.size(); i++) {
			_grille.add(new Cellule(grille.get(i),i+1));
		}
	}
	
	public Cellule getNextCelluleToFill() {
		System.out.println("Cellules to fill length : " + _cellulesToFill.size());
		if (_cellulesToFill.size() == 0) {
			System.out.println("Size 0");
			return new Cellule('a');
		}
		System.out.println("Size > 0");
		return _cellulesToFill.get(0);
	}
	
	public Cellule getCelluleAtIndex(int index) {
		return _grille.get(index);
	}
	
	public List<Cellule> getGrille() {
		return _grille;
	}
	
	public void trierGrille(List<Cellule> grille) {
		Collections.sort(grille, new PossibilitiesCountComparator<Cellule>());
	}
	
	public void trierCellulesToFill() {
		trierGrille(_cellulesToFill);
	}
	
	public boolean hasMissingCases() {
		if (_endMode)
			return false;
		if (_cellulesToFill.size() == 0)
			return false;
		return true;
	}
	
	public void endSudoku() {
		_endMode = true;
	}
	
	public boolean isSudokuEnded() {
		return _endMode;
	}
	
	public void initializePossibilities() {
		for (int i=0; i<_grille.size(); i++) {
			Cellule cell = _grille.get(i);
			if (!cell.hasCharacter()) {
				_cellulesToFill.add(cell);
			}
		}
		for (int i=0; i<_cellulesToFill.size(); i++) {
			createPossibilites(_cellulesToFill.get(i));
		}
		trierGrille(_cellulesToFill);
	}
	
	public boolean checkIfCharIsGood(Cellule cell, char aChar) {
		int index = cell.getIndex() - 1;
		Set<Integer> aSet = new HashSet<Integer>();
		int[] rowIndexes = getRowForIndex(index);
		int[] colIndexes = getColumnForIndex(index);
		int[] regIndexes = getRegionForIndex(index);
		for (int i=0; i<9; i++) {
			aSet.add(rowIndexes[i]);
			aSet.add(colIndexes[i]);
			aSet.add(regIndexes[i]);
		}
		
		Set<Character> possibilities = new TreeSet<Character>();
		possibilities.add('1');
		possibilities.add('2');
		possibilities.add('3');
		possibilities.add('4');
		possibilities.add('5');
		possibilities.add('6');
		possibilities.add('7');
		possibilities.add('8');
		possibilities.add('9');
		
		Iterator<Integer> iter = aSet.iterator();
		while (iter.hasNext()) {
			int currIndex = iter.next();
			Cellule toCompare = _grille.get(currIndex);
			if (toCompare.hasCharacter()) {
				possibilities.remove(toCompare.getCharacter());
			}
		}
		return checkIfElemIsInSet(possibilities, aChar);
	}
	
	private boolean checkIfElemIsInSet(Set<Character> set, Character elem) {
		int oldLength = set.size();
		set.remove(elem);
		int newLength = set.size();
		return (oldLength == newLength);
	}
	
	private void createPossibilites(Cellule cell) {
		int index = cell.getIndex() - 1;
		Set<Integer> aSet = new HashSet<Integer>();
		int[] rowIndexes = getRowForIndex(index);
		int[] colIndexes = getColumnForIndex(index);
		int[] regIndexes = getRegionForIndex(index);
		for (int i=0; i<9; i++) {
			aSet.add(rowIndexes[i]);
			aSet.add(colIndexes[i]);
			aSet.add(regIndexes[i]);
		}
		Iterator<Integer> iter = aSet.iterator();
		while (iter.hasNext()) {
			int currIndex = iter.next();
			Cellule toCompare = _grille.get(currIndex);
			if (toCompare.hasCharacter()) {
				cell.removePossibility(toCompare.getCharacter());
			}	
		}
	}
	
//	private boolean characterIsValid(char character, Cellule cell) {
//		int index = cell.getIndex()-1;
//		Cellule[] row = getRowForIndex(index);
//		return false;
//	}
	
	public boolean directCheck() {
		Cellule cell = _cellulesToFill.get(0);
		if (cell.getPossibilityCount() == 1) {
			cell.assignPossibility();
			refreshNeighbouringCells(cell);
			trierGrille(_cellulesToFill);
			_cellulesToFill.remove(cell);
			return true;
		}
		return false;
	}
	
	public boolean neighbourCheck() {
		for (int i=0; i<9; i++) {
			if (rowCheck(i))
				return true;
			if (columnCheck(i))
				return true;
//			if (regionCheck(i))
//				return true;
		}
		return false;
	}
	
	public boolean rowCheck(int rowNumber) {
		Cellule[] cellulesInRow = getCellulesFromArray(getRowForIndex(rowNumber*9));
		for (int i=0; i<cellulesInRow.length; i++) {
			char match = comparisonCheck(cellulesInRow[i], cellulesInRow);
			if (match != 'a') {
				Cellule currCell = cellulesInRow[i];
				currCell.assignPossibility(match);
				refreshNeighbouringCells(currCell);
				trierGrille(_cellulesToFill);
				_cellulesToFill.remove(currCell);
				return true;
			}
		}
		return false;
	}
	
	public boolean columnCheck(int columnNumber) {
		Cellule[] cellulesInColumn = getCellulesFromArray(getColumnForIndex(columnNumber));
		for (int i=0; i<cellulesInColumn.length; i++) {
			char match = comparisonCheck(cellulesInColumn[i], cellulesInColumn);
			if (match != 'a') {
				Cellule currCell = cellulesInColumn[i];
				currCell.assignPossibility(match);
				refreshNeighbouringCells(currCell);
				trierGrille(_cellulesToFill);
				_cellulesToFill.remove(currCell);
				return true;
			}
		}
		return false;
	}
	
//	public boolean regionCheck(int regionNumber) {
//		Cellule[] cellulesInRow = getCellulesFromArray(getRowForIndex(rowNumber*9));
//		for (int i=0; i<cellulesInRow.length; i++) {
//			char match = comparisonCheck(cellulesInRow[i], cellulesInRow);
//			if (match != 'a') {
//				Cellule currCell = cellulesInRow[i];
//				currCell.assignPossibility(match);
//				refreshNeighbouringCells(currCell);
//				trierGrille(_cellulesToFill);
//				_cellulesToFill.remove(currCell);
//				return true;
//			}
//		}
//		return false;
//	}
	
	public char comparisonCheck(Cellule aCell, Cellule[] neighbouringCells) {
		List<Character> possibilities = aCell.getPossibilitiesAsList();
		Set<Character> onOtherCells = new TreeSet<Character>();
		for (int i=0; i<neighbouringCells.length; i++) {
			Cellule currentCell = neighbouringCells[i];
			if (currentCell.getIndex() != aCell.getIndex()) {
				List<Character> currCellPossibilities = currentCell.getPossibilitiesAsList();
				for (int j=0; j<currCellPossibilities.size(); j++) {
					onOtherCells.add(currCellPossibilities.get(j));
				}
				Iterator<Character> iter = onOtherCells.iterator();
				while (iter.hasNext()) {
					char currChar = iter.next();
					possibilities.remove(currChar);
				}
				if (possibilities.size() == 1)
					return possibilities.get(0);
			}
		}
		return 'a';
	}
	
	public void refreshNeighbouringCells(Cellule cell) {
		int index = cell.getIndex() - 1;
		Cellule[] cellulesInRow = getCellulesFromArray(getRowForIndex(index));
		removeAllPossibilities(cellulesInRow, cell.getCharacter());
		Cellule[] cellulesInColumn = getCellulesFromArray(getColumnForIndex(index));
		removeAllPossibilities(cellulesInColumn, cell.getCharacter());
		Cellule[] cellulesInRegion = getCellulesFromArray(getRegionForIndex(index));
		removeAllPossibilities(cellulesInRegion, cell.getCharacter());
	}
	
	public void removeAllPossibilities(Cellule[] cellules, char toRemove) {
		for (int i=0; i<cellules.length; i++) {
			cellules[i].removePossibility(toRemove);
		}
	}
	
	private Cellule[] getCellulesFromArray(int[] array) {
		Cellule[] cellules = new Cellule[array.length];
		for (int i=0; i<cellules.length; i++) {
			cellules[i] = _grille.get(array[i]);
		}
		return cellules;
	}
	
	private int[] getRowForIndex(int index) {
		int[] row = new int[9];
		for (int i=0; i<9; i++) {
			int rowPos = ((index - index%9)/9);
			row[i] = (rowPos)*9+i;
		}
		return row;
	}
	
	private int[] getColumnForIndex(int index) {
		int[] column = new int[9];
		for (int i=0; i<9; i++) {
			int columnPos = index%9;
			column[i] = (columnPos)+9*i;
		}
		return column;
	}
	
	private int[] getRegionForIndex(int index) {
		int[] region = new int[9];
		for (int i=0; i<9; i++) {
			int regionPos = (index/27 * 3 + (index%9)/3);
			region[i] = regionPos/3 * 27 + i/3 * 9 + regionPos%3 * 3 + i%3;
		}
		return region;
	}
	
	public String toString() {
		String toReturn = "";
		for (int i=0; i<9; i++) {
			for (int j=0; j<9; j++) {
				toReturn = toReturn + _grille.get(i*9+j);
			}
			toReturn = toReturn + "\n";
		}
		return toReturn;
	}
}
