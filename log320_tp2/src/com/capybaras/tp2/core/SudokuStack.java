package com.capybaras.tp2.core;

import java.util.ArrayList;
import java.util.List;

public class SudokuStack {
	
	List<Sudoku> sudokus;
	
	public SudokuStack() {
		sudokus = new ArrayList<Sudoku>();
	}
	
	public void push(Sudoku state) {
		sudokus.add(state);
	}
	
	public Sudoku pop() {
		if (sudokus.isEmpty())
			return null;
		Sudoku toReturn = sudokus.get(sudokus.size() - 1);
		sudokus.remove(sudokus.size() - 1);
		return toReturn;
	}
	
	public boolean isEmpty() {
		return sudokus.isEmpty();
	}
	
	public int size() {
		return sudokus.size();
	}

}
