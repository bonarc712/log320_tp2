package com.capybaras.tp2.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.capybaras.tp2.core.ResolverController;
import com.capybaras.tp2.core.Sudoku;
import com.capybaras.tp2.fileutil.SudokuFileReader;


public class SudokuApp implements ActionListener {

	private Sudoku _sudoku;
	private JFrame _frame;
	private JFileChooser _fileChooser;
	private JTextField _filepath;
	private JButton _browseButton;
	private JButton _solveButton;
	private JButton _exitButton;
	
	private boolean _hasSudoku = false;
	
	
	public SudokuApp() {		
		_sudoku = new Sudoku();
		initializeUI();
	}	
	
	public void initializeUI() {
		
		JPanel panel = new JPanel(new BorderLayout());
		_frame = new JFrame("Sudoku solver");
		JPanel topPanel = new JPanel();
		JPanel centerPanel = new JPanel();
		
		JLabel label = new JLabel("File path: ");
		_filepath = new JTextField("");
		_browseButton = new JButton("Browse");
		_solveButton = new JButton("Solve");
		_exitButton = new JButton("Exit");
		
		_filepath.setPreferredSize(new Dimension(200, 30));
		_filepath.setEnabled(false);
		
		_browseButton.addActionListener(this);
		_solveButton.addActionListener(this);
		_exitButton.addActionListener(this);
		
		_fileChooser = new JFileChooser();
		
		_frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		_frame.setLayout(new FlowLayout());
		
		
		topPanel.add(label);
		topPanel.add(_filepath);
		topPanel.add(_browseButton);
		centerPanel.add(_solveButton);
		centerPanel.add(_exitButton);
		panel.add(topPanel, BorderLayout.PAGE_START);
		panel.add(centerPanel, BorderLayout.CENTER);
		
		_frame.setPreferredSize(new Dimension(400, 120));
		_frame.setContentPane(panel);
		_frame.pack();
		_frame.setLocationRelativeTo(null);
		_frame.setVisible(true);
	}
	
	public void showOptions() {
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		if(action.getSource() == _browseButton) {
			int returnVal = _fileChooser.showOpenDialog(null);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				try {
					String filepath = _fileChooser.getSelectedFile().getAbsolutePath();
					_filepath.setText(filepath);
					ArrayList<Character> list = SudokuFileReader.readFile(filepath);
					_sudoku.setGrille(list);
					
					Sudoku test = new Sudoku(_sudoku);
					_hasSudoku = true;
				}
				catch(IOException e) {
					System.out.println("Erreur lors de la fermeture du fichier sudoku.");
				}
				_filepath.setText(_fileChooser.getSelectedFile().getAbsolutePath());
			}
		}
		if(action.getSource() == _solveButton) {
			if (_hasSudoku) {
				ResolverController resolver = new ResolverController(_sudoku);
				resolver.resolve();
			}
			else {
				System.out.println("Erreur, aucune grille trouv�e!");
			}
		}
		else if(action.getSource() == _exitButton) {
			_frame.dispose();
		}
		
	}

}
